﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace np_hw6
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void ButtonDownloadClick(object sender, RoutedEventArgs e)
        {
            ThreadPool.QueueUserWorkItem(DownloadFileRoutine);
           
        }

        private void DownloadFileRoutine(object state)
        {
            using (WebClient wc = new WebClient())
            {
                string proxy = "";
                Dispatcher.Invoke(() => { proxy = textBoxProxy.Text; });
                WebProxy wp = new WebProxy(proxy);
                if (proxy != "0.0.0.0") wc.Proxy = wp;
                string url = "";
                string filename = "";
                Dispatcher.Invoke(() => { url = textBoxDownLoadUri.Text; });
                Dispatcher.Invoke(() => { filename = textBoxFileName.Text; });
                wc.DownloadFileAsync(new Uri(url),filename);
                wc.DownloadDataCompleted+=DownloadCompleted;
            }
        }

        private void DownloadCompleted(object sender, DownloadDataCompletedEventArgs e)
        {
            MessageBox.Show("Download Completed");
        }
    }
}
